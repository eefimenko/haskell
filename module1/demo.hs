-- Calculate Fibonacci numbers, straightforward implementation

fibonacci :: Integer -> Integer
fibonacci n | n == 0 = 0
            | n == 1 = 1
            | n == -1 = 1
            | n > 1 = fibonacci(n - 1) + fibonacci(n - 2)
            | n < -1 = fibonacci(n + 2) - fibonacci(n + 1)

-- Calculate Fibonacci numbers, tail recursion

fibonacci' :: Integer -> Integer
fibonacci' n | n > 1    = helper 0 1 (n - 1)
             | n == 1   = 1
             | n == 0   = 0
             | n == -1  = 1
             | n < (-1) = helper (-1) 0 (n)
             

helper x y 1 = x + y
helper x y (-1) = y - x
helper x y n | n >= 0 = helper y (x + y) (n - 1)  
             | n < 0 = helper y (x - y) (n + 1)

-- Calculate sequence using recursive formula

seqA :: Integer -> Integer

seqA n | n == 0 = 1
       | n == 1 = 2
       | n == 2 = 3
       | n > 2 = seqA_helper 1 2 3 n
       | otherwise = error "Wrong number"

seqA_helper x y z 3 = z + y - 2 * x

seqA_helper x y z n = seqA_helper y z (z + y - 2 * x) (n - 1)

-- Rewrite sequence using let .. in
                      
seqA' :: Integer -> Integer

seqA' n | n == 0 = 1
        | n == 1 = 2
        | n == 2 = 3
        | n > 2 = let
            helper x y z 3 = z + y - 2 * x
            helper x y z n = helper y z (z + y - 2 * x) (n - 1)
          in helper 1 2 3 n
        | otherwise = error "Wrong number"

-- Write function that calculates sum of digits and number of digits

sum'n'count :: Integer -> (Integer, Integer) 

sum'n'count 0 = (0, 1)
sum'n'count n = let
                  shelper x y 0 = (x, y)
                  shelper x y r = let c = mod r 10
                    in shelper (x + c) (y + 1) (div r 10)
                in shelper 0 0 (abs n)

-- Write function that integrates function on [a, b] interval

integration :: (Double -> Double) -> Double -> Double -> Double
integration f a b = helper f 0 a n
  where
    n = 1000
    dx = (b - a) / n
    helper f s x 0 = s * dx
    helper f s x n = helper f (s + ds) (x + dx) (n - 1)
      where
        ds = (f x + f (x + dx)) / 2

