module Demo where 

-- Triple operator
on3 :: (b -> b -> b -> c) -> (a -> b) -> a -> a -> a -> c
on3 op f x y z = op (f x) (f y) (f z)

-- Composition of functions (composition operator)
compose f g = \x -> f (g x)

-- 1. Max (x, 42) 2. ^3 3. log
doItYourself = f . g . h

f = logBase 2

g = (^3)

h = max 42

-- Type classes
{-
class Eq a where
  (==) :: a -> a -> Bool
  (/=) :: a -> a -> Bool
  x /= y = not (x == y)
  x == y = not (x /= y)

instance Eq Bool where
  True  == True  = True
  False == False = True
  _     == _     = False

instance (Eq a, Eq b) => Eq (a, b) where
  p1 == p2 = fst p1 == fst p2 && snd p1 == snd p2
-}

class Printable a where
  toString :: a -> [Char]

instance Printable Bool where
  toString True = "true"
  toString False = "false"

instance Printable () where
  toString _ = "unit type"

instance (Printable a, Printable b) => Printable (a,b) where
  toString p = "(" ++ toString (fst p) ++ "," ++ toString (snd p) ++ ")"
